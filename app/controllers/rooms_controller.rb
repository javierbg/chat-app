class RoomsController < ApplicationController
  before_action :logged_in_user

  def new
    @room = Room.new
  end

  def create
    @room = Room.new(name: params[:room][:name])
    if @room.save
      redirect_to room_path(@room)
    else
      redirect_to rooms_path
    end
  end

  def index
    @rooms = Room.all
  end

  def show
    @room = Room.find(params[:id])
    @messages = @room.messages.all
    @message = @room.messages.build
  end
end
