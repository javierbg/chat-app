module UsersHelper
  def current_user
    if (user_token = session[:user_token])
      @current_user ||= User.find_by(token: user_token)
    end
  end

  def logged_in?
    !current_user.nil?
  end
end
