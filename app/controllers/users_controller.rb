class UsersController < ApplicationController
  def new
    if logged_in?
      redirect_to rooms_path
    end
  end

  def create
    @user = User.new(name: params[:user][:name])
    if @user.save
      session[:user_token] = @user.token
      redirect_to rooms_path
    else
      redirect_to root_url
    end
  end
end
