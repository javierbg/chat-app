require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "POST create" do
    it "rejects invalid names" do
      post :create, params: { user: { name: "" } }
      expect(response).to redirect_to(root_path)
    end

    it "redirects to the rooms index when the user is successfully created" do
      post :create, params: { user: { name: "Test user" } }
      expect(response).to redirect_to(rooms_path)
    end
  end

  describe "GET new" do
    it "redirects to the rooms index when already logged in" do
      post :create, params: { user: { name: "Test user" } }
      get :new
      expect(response).to redirect_to(rooms_path)
    end
  end
end
