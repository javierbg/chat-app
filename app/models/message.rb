class Message
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  field :content, type: String

  validates :content, presence: true, length: { maximum: 140 }

  belongs_to :room, inverse_of: :messages
  belongs_to :user, inverse_of: nil

  default_scope -> { order(created_at: :asc) }
end
