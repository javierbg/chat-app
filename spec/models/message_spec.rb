require 'rails_helper'

RSpec.describe Message, type: :model do
  before(:each) do
    @user = User.create!(name: "user1")
    @room = Room.create!(name: "room")
  end

  context "creation" do
      it "rejects empty messages" do
        message = @room.messages.create(content: "", user: @user)
        expect(message.save).to be false
      end

      it "rejects messages with no user" do
        message = @room.messages.create(content: "content")
        expect(message.save).to be false
      end
  end
end
