require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  describe "GET index" do
    it "redirects to root when not logged in" do
      get :index
      expect(response).to redirect_to(root_url)
    end
  end

  describe "POST create" do
    describe "when logged in" do
      before(:each) do
        user = User.create!(name: "Test User")
        login(user)
      end

      it "rejects rooms with invalid names" do
        post :create, params: { room: { name: "" } }
        expect(response).to redirect_to(rooms_path)

        post :create, params: { room: { name: "a"*51 } }
        expect(response).to redirect_to(rooms_path)
      end

      it "redirects to the newly created room" do
        room_name = "Test room"
        post :create, params: { room: { name: room_name } }
        expect(response).to redirect_to(room_path(assigns(:room)))
      end
    end

    describe "when not logged in" do
      it "doesn't let not logged-in users to create rooms" do
        post :create, params: { room: { name: "Test room" } }
        expect(response).to redirect_to(root_url)
      end
    end
  end
end
