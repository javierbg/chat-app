module SpecTestHelper
  def login(user)
    request.session[:user_token] = user.token
  end

  def current_user
    User.find(request.session[:user_token])
  end
end
