Pequeña aplicación de chat implementada en [Ruby on Rails](https://rubyonrails.org/) para aprender a utilizar esta tecnología

Hace uso de:
* [Action Cable](https://github.com/rails/rails/tree/master/actioncable) para la comunicación en tiempo real mediante WebSockets
* [Rspec](https://github.com/rspec/rspec-rails) para los tests (BDD)
* [Mongo](https://www.mongodb.com/)/[Mongoid](https://github.com/mongodb/mongoid) como base de datos
* [Vue.js](https://vuejs.org/) para el frontend
