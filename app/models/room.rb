class Room
  include Mongoid::Document
  field :name, type: String

  validates :name, presence: true, length: { maximum: 50 }
  index({ name: 1 }, { unique: true })

  has_many :messages
end
