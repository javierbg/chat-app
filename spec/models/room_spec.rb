require 'rails_helper'

RSpec.describe Room, type: :model do
  before(:each) do
    @user1 = User.create!(name: "user1")
    @user2 = User.create!(name: "user2")
    @room = Room.create!(name: "room")
  end

  describe "#messages" do
    context "with more than one message" do
      before(:each) do
        # Create 20 messages from the two users
        @messages = (1..20).collect do |i|
          user = (i%2)==0 ? @user1 : @user2
          @room.messages.create!(content: "hi", user: user)
        end
      end

      it "retrieves them in chronological order" do
        expect(@room.messages).to eq(@messages)
      end
    end
  end
end
