class MessagesController < ApplicationController
  before_action :logged_in_user

  def create
    @room = Room.find(params[:room_id])
    @message = @room.messages.build(content: params[:message][:content], user: current_user)
    @message.save
    redirect_to room_path(@room)
  end
end
