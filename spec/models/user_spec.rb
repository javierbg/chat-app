require 'rails_helper'

RSpec.describe User, type: :model do
  describe "creation" do
    context "when creating a new user session" do
      before(:each) do
        @user = User.new(name: "Test user")
      end

      it "rejects empty usernames" do
        @user[:name] = ""
        expect(@user.save).to be false
      end

      it "rejects too long usernames" do
        @user[:name] = "a"*51
        expect(@user.save).to be false
      end

      it "assigns them a new base64 token" do
        @user.save
        expect(@user[:token]).to_not be_nil
        expect(@user[:token]).to match(/([A-Za-z0-9\+\/=])+/)
      end

      it "protects the token to be read-only" do
        @user.save
        expect { @user.token = SecureRandom.base64(21) }.to raise_error(Mongoid::Errors::ReadonlyAttribute)
      end
    end
  end

  describe "search" do
    context "when looking for a user" do
      it "can be found by its session token" do
        user = User.new(name: "Test user")
        user.save
        expect(User.find_by(token: user.token)).to eq(user)
      end
    end
  end
end
