require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  describe "POST create" do
    before(:each) do
      @room = Room.create!(name: "Test room")
    end

    describe "when logged in" do
      before(:each) do
        user = User.create!(name: "Test User")
        login(user)
      end

      it "redirects to the room after sending the message" do
        post :create, params: { room_id: @room.id, message: {content: "test" } }
        expect(response).to redirect_to(room_path(@room))
      end
    end

    describe "when not logged in" do
      it "redirects to root" do
        post :create, params: { room_id: @room.id, message: {content: "test" } }
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
