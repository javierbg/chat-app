class User
  include Mongoid::Document
  field :name, type: String
  field :token, type: String

  has_many :messages

  validates :name, presence: true, length: { maximum: 50 }
  validates :token, presence: true, length: { minimum: 28 }

  index({token: 1}, {unique: true})

  attr_readonly :token

  before_validation do |user|
    user[:token] = SecureRandom.base64(21)
  end
end
