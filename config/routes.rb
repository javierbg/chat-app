Rails.application.routes.draw do
  root to: 'users#new'

  resources :users, only: [:new, :create]
  resources :rooms, only: [:index, :new, :create, :show]

  resources :rooms do
    resources :messages, only: [:create]
  end
end
